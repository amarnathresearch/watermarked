#!/usr/bin/env python
# coding: utf-8

# In[11]:


import sys

from google.cloud import automl_v1beta1
from google.cloud.automl_v1beta1.proto import service_pb2


# In[12]:


def main():
    file_path = 'images/watermarked/fotolia_137840668.jpg'
    project_id = 'mytestproject-1487674567360'
    model_id = 'ICN6142152273688773413'

    with open(file_path, 'rb') as ff:
        content = ff.read()
    respose = get_prediction(content, project_id,  model_id) 
    return response

response = main()
print(response)

def get_prediction(content, project_id, model_id):
  prediction_client = automl_v1beta1.PredictionServiceClient()

  name = 'projects/{}/locations/us-central1/models/{}'.format(project_id, model_id)
  payload = {'image': {'image_bytes': content }}
  params = {}
  request = prediction_client.predict(name, payload, params)
  return request  # waits till request is returned


# In[ ]:




